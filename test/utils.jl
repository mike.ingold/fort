using RefractRT
using Test

@testset "RefractRT utils.jl" begin
	# All axes range (-100,100)
	dim = collect(range(-100, 100, length=101))
	# Refractive index = 1.0 at origin, increases to 2.0 at extremes
	n(x,y,z) = 1.0 + sqrt((x/100.0)^2 + (y/100)^2 + (z/100)^2)
	# Build model with these specs
	model = RayTraceModel("Testing","mm",dim;n=n)
	model_origin = Vector{Float64}([0,0,0])

	@test RefractRT.isvalid(model)
	@test RefractRT.lookupRefIndex(model, model_origin) == 1.0
end

