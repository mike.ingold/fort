using Pkg
# Pkg.activate("/home/mike/RefractRT")
DIR_ROOT = dirname(@__DIR__)
Pkg.activate(DIR_ROOT)
using RefractRT

using Dates
using LinearAlgebra

using Test

@testset "RayTraceModel{Float32}, addray, size, length" begin
	dim = Float32.(range(-1,1,length=1001))
	f(t)::Float32 = 0.05 * exp(-5*t) * cos(100*t) + 1
	n(x,y,z)::Float32 = f(norm([x,y,z]))
	o_pos = Vector{Float32}([0,0,0])
	o_dir = Vector{Float32}([0,0,1])

	model = RayTraceModel("Test Model", "millimeters", dim; n=n)
	addray(model, o_pos, o_dir)

	@test model isa RayTraceModel{Float32}
	@test size(model) == (1001, 1001, 1001)
	@test length(model.rays) == 1
	@test length(model.rays[1]) > 0
end
