# Needed Features

* Visualization of ray paths traversing a medium
	* Plotting series of 3D points should be easy enough
	* Also possible to colorize n gradient?
	* Colorizing might only be possible on a 2D slice/projection
* Visualizing the field itself
	* Interactive cross-sections?

# Refactoring:

* Create new RayTraceModel struct for lazy evaluation
	* Stores an n::Function for analytic refractive index
	* n isa Vector{Function}?
	* Calculate gradient directly with grad(f(x,y,z))

# Improvements:

* Look for efficiencies in Ray storage
* Look for more efficient N field map generation
	* Comprehension technique @btime (laptop) -> 70.5s, 93.4GB
	* Instantiating Array{}(undef) and .='ing it -> 72.0s, 97.2GB
* Parallelize Ray tracing
	* Possible to vectorize (SSE/AVX/SIMD) instructions?
	* Integrate CUDA.jl as an option
* Create a file format standard for saving/recalling data
	* This can workaround RAM limitations for large model runs
	* This + advanceall (above) could enable distributed computing

