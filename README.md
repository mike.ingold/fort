# RefractRT.jl

RefractRT is a modeling & simulation engine for ray-tracing photon paths as they refract through a voxelized physical medium.

## Example Usage

```julia
julia> using RefractRT

julia> dim = collect(range(-1, 1, length=100))
100-element Array{Float64,1}:
 -1.0
 -0.9797979797979798
 -0.9595959595959596
 -0.9393939393939394
 -0.9191919191919192
  ⋮
  0.9191919191919192
  0.9393939393939394
  0.9595959595959596
  0.9797979797979798
  1.0

julia> n(x,y,z) = 1.0 + sqrt(x^2 + y^2 + z^2)
n (generic function with 1 method)

julia> model = RayTraceModel("Example", "millimeters", dim; n=n)
RayTraceModel:
	ID: Example
	UNITS: millimeters
        X: -1.0:1.0
        Y: -1.0:1.0
        Z: -1.0:1.0
```

