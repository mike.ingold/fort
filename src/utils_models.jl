"""
	addray(model, pos, dir)

Generate a new Ray within a `RayTraceModel` beginning at a given `position`
and `direction`.
"""
function addray(model::RayTraceModel{T}, pos::Vector{T}, dir::Vector{T}) where {T<:AbstractFloat}
	ray = Ray(pos,dir)
	while !ray.terminated
		advance(model, ray)
	end
	push!(model.rays, ray)
end

"""
	addplanerays(model, plane; reversed, run)

Add an array of Ray's to a `RayTraceModel` along a specified `plane`.

Plane must be selected from: `:XY`, `:XZ`, or `:YZ`.

Optional Arguments:
* `reversed`: propagate rays in the direction of the specified plane's negative normal vector
* `run`: automatically advance created Ray's until they terminate
"""
function addplanerays(model::RayTraceModel{T}, plane::Symbol;
		      reversed::Bool=false, run::Bool=true) where {T<:AbstractFloat}
	# Interpret input arguments
	# TODO remove? T = typeof(model.X[1])

	# Generate direction vector and an array of start positions
	mode = uppercase(string(plane, (reversed ? "-" : "+")))
	if mode == "XY+"
		dir = Vector{T}([0,0,1])
		posarray = [ Vector{T}([x, y, model.Z[1]]) for x in model.X, y in model.Y ]
	elseif mode == "XY-"
		dir = Vector{T}([0,0,-1])
		posarray = [ Vector{T}([x, y, model.Z[end]]) for x in model.X, y in model.Y ]
	elseif mode == "XZ+"
		dir = Vector{T}([0,1,0])
		posarray = [ Vector{T}([x, model.Y[1], z]) for x in model.X, z in model.Z ]
	elseif mode == "XZ-"
		dir = Vector{T}([0,-1,0])
		posarray = [ Vector{T}([x, model.Y[end], z]) for x in model.X, z in model.Z ]
	elseif mode == "YZ+"
		dir = Vector{T}([1,0,0])
		posarray = [ Vector{T}([model.X[1], y, z]) for y in model.Y, z in model.Z ]
	elseif mode == "YZ-"
		dir = Vector{T}([-1,0,0])
		posarray = [ Vector{T}([model.X[end], y, z]) for y in model.Y, z in model.Z ]
	else
		throw(ArgumentError("Invalid plane $(plane). Select from :XY, :XZ, :YZ"))
	end

	# Append instantiated Rays to model
	@debug "Instantiating basic Rays"
	i = length(model.rays)+1
	append!(model.rays, [ Ray(pos,dir) for pos in posarray ])

	# Auto-advance all Ray's if run argument is enabled
	if run
		advanceallrays(model; i=i)
	end
end

"""
	advanceallrays(model::RayTraceModel; i=1)

Advance all Rays in a `RayTraceModel` until they terminate. Optional argument
`i` indicates that only model.rays[i:end] should be advanced.
"""
function advanceallrays(model::RayTraceModel; i=1)
	@info "Advancing all Rays in $(model.ID) using $(Threads.nthreads()) CPU threads"
	Threads.@threads for ray in model.rays[i:end]
		@debug "Propagating a Ray in $(model.ID) ray[($i)]"
		while !ray.terminated
			advance(model,ray)
		end
	end
end

"""
	clone(model::RayTraceModel; all=true)

Create an independent duplicate of a `RayTraceModel`. If `all is enabled` the
returned copy will be completely identical to the provided model, otherwise
the returned copy will have empty `notes` and `rays` containers.
"""
function clone(model::RayTraceModel; all=true)
	if all
		# Return an independent but fully-duplicated RayTraceModel
		return deepcopy(model)
	else
		# Return a new RayTraceModel with empty .notes and .rays
		return RayTraceModel(
				     model.ID,
				     model.UNITS,
				     model.X,
				     model.Y,
				     model.Z,
				     model.N,
				     Vector{String}(),
				     Vector{Ray{T}}()
				    )
	end
end

