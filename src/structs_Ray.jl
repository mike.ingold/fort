"""
	Ray{T<:AbstractFloat}

Representation of a photon's ray path through a modeled space.

# Fields
* `position`::`Vector{T}` a map of spacetime coordinates representing the Ray path
* `direction`::`Vector{T}` a vector pointing representing the ray's orientation
* `terminated`::`Bool` indicates whether the Ray is finished being traced
"""
mutable struct Ray{T<:AbstractFloat}
	position::Vector{Vector{T}}
	direction::Vector{Vector{T}}
	terminated::Bool
end

"""
	Ray(pos, dir)

Construct a Ray with origin `position` and `direction`.
"""
function Ray(pos::Vector{T}, dir::Vector{T}) where {T<:AbstractFloat}
	return Ray([pos], [dir], false)
end

"""
	length(ray::Ray)

Return the number of position/direction pairs in a Ray trace.
"""
function Base.length(ray::Ray)
	# Check the lengths of ray's position and direction fields
	len_pos = length(ray.position)
	len_dir = length(ray.direction)

	# If lengths are identical, return the length
	if len_pos == len_dir
		return len_pos
	else
		throw(ErrorException("Ray has a position/direction length mismatch"))
	end
end

# Custom pretty-printing for Ray structs
function Base.show(io::IO, ray::Ray)
	if length(ray.position) > 0
		s1 = "Ray with origin $(ray.position[1])"
	else
		s1 = "Ray with no origin"
	end

	s2 = ray.terminated ? ", terminated." : ", unterminated"
	print(io, string(s1,s2))
end

