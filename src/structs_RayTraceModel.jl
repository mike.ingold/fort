"""
	RayTraceModel{T<:AbstractFloat}

Representation of a modeled voxelized medium with refractive index properties.
Refractive indices are mapped to from each range value toward limit of next
range value.

# Fields
* `ID`::`String` identifying name of this model
* `UNITS`::`String` units of measure for distances, e.g. "mm" or "in"
* `X`::`Vector{T}` range along x-dimension (monotonic-increasing)
* `Y`::`Vector{T}` range along y-dimension (monotonic-increasing)
* `Z`::`Vector{T}` range along z-dimension (monotonic-increasing)
* `N`::`Array{T,3}` refractive index for each voxel with locations
* `notes`::`String` container to hold text notes about this model
* `rays`::`Vector{Ray{T}}` container to hold Ray objects
"""
mutable struct RayTraceModel{T<:AbstractFloat} 
	# Properties not intended to change
	ID::String
	UNITS::String
	X::Vector{T}
	Y::Vector{T}
	Z::Vector{T}
	N::Array{T,3} # Refractive index

	# Properties intended to change
	notes::Vector{String}
	rays::Vector{Ray{T}}
end

"""
	RayTraceModel(id, units, x, y, z; n=ones)

Construct a RayTraceModel with an `identifier`, `units`, and `axis ranges`
specification. Voxels are initialized by default with a refractive index of 1,
but can be specified with the optional argument `n`.

# Optional argument `n`
* **Base.ones**: n(x,y,z) = 1`
* `undef`: allocate an array but don't assign values yet
* `n(x,y,z)`: provided analytic function
"""
function RayTraceModel(
		        id::String,
		        units::String,
		        X::Vector{T},
		        Y::Vector{T},
		        Z::Vector{T};
		        n=Base.ones
		      ) where (T<:AbstractFloat)

	# Get size of array for allocation
	space_size = [ length(X), length(Y), length(Z) ]

	# Initialize refractive index array
	if n == Base.ones
		@info "Initializing N array with default value 1"
		ref_index = Base.ones(T, space_size...)
	elseif n == undef
		@info "Initializing N array with undefined values"
		ref_index = Array{T,3}(undef, space_size...)
	elseif n isa Function
		@info "Using custom n function; n(0,0,0) = $(n(0,0,0))"
		ref_index::Array{T,3} = [ n(x,y,z)::T for x in X, y in Y, z in Z ]
	else
		throw(ArgumentError("Arg n must be a function(x,y,z), Base.ones, or undef"))
	end

	return RayTraceModel(id,                 # model.ID
			     units,              # model.UNITS
			     X,                  # model.X
			     Y,                  # model.Y
			     Z,                  # model.Z
			     ref_index,          # model.N
			     Vector{String}(),   # model.notes
			     Vector{Ray{T}}()  ) # model.rays
end

"""
	RayTraceModel(id, units, dim; n)

Construct a RayTraceModel with an `identifier`, `units`, and a single `axis
range` specification. Voxels are initialized by default with a refractive index
of 1, but can be specified with the optional argument `n`.

# Optional argument `n`
* **Base.ones**: n(x,y,z) = 1
* `undef`: allocate an array but don't assign values yet
* `n(x,y,z)`: provided analytic function
"""
function RayTraceModel(
		        id::String,
			units::String,
			dim::Vector{T};
		        n=undef
		      ) where {T<:AbstractFloat}

	return RayTraceModel(id, units, dim, dim, dim; n=n)
end

# Custom pretty-printing for RayTraceModel
function Base.show(io::IO, model::RayTraceModel)
	println(io, "RayTraceModel")
	println(io, "\tID: $(model.ID)")
	println(io, "\tUNITS: $(model.UNITS):")
	println(io, "\tX: $(model.X[1]):$(model.X[end])")
	println(io, "\tY: $(model.Y[1]):$(model.Y[end])")
	print(io, "\tZ: $(model.Z[1]):$(model.Z[end])")
end

"""
	size(model::RayTraceModel)

Return a tuple containing the dimensions of `a model`. Optionally you can specify
a dimension to just get the length of that dimension.
"""
function Base.size(model::RayTraceModel, dim=0)
	if dim == 0
		return ( length(model.X), length(model.Y), length(model.Z) )
	elseif dim == 1
		return length(model.X)
	elseif dim == 2
		return length(model.Y)
	elseif dim == 3
		return length(model.Z)
	else
		throw(ArgumentError("Invalid model dimension $(dim)"))
	end
end

