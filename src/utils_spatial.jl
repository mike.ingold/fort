"""
	getAdjBounds(value, range)

Find the values in a given `range` that are adjacent but non-equal to a
`specified value`. When no values in the range exist on one side of the given
value, NaN will be returned in that position.
"""
function getAdjBounds(value::T, range::Vector{T}) where {T<:Number}
	i_lower = findlast(range .< value)
	i_upper = findfirst(range .> value)
	bound_lower = isnothing(i_lower) ? NaN : range[i_lower]
	bound_upper = isnothing(i_upper) ? NaN : range[i_upper]
	return [ bound_lower bound_upper ]
end

"""
	isOutOfBounds(model::RayTraceModel, position)

Checks if the `specified position` is within the bounds of the `given RayTraceModel`.
"""
function isOutOfBounds(model::RayTraceModel{T}, pos::Vector{T})::Bool where {T<:AbstractFloat}
	# Optimized generic function: assumes range is monotonic-increasing
	iswithin(thing, range) = (thing>=range[1]) && (thing<=range[end])
	return !iswithin(pos[1], model.X) ||
		!iswithin(pos[2], model.Y) ||
		!iswithin(pos[3], model.Z)
end

"""
	lookupVoxel(model::RayTraceModel, position)

Lookup the voxel index in a `RayTraceModel` that contains a `position`.
"""
function lookupVoxel(model::RayTraceModel{T}, pos::Vector{T}) where {T<:AbstractFloat}
	f(value, space) = findfirst(value .<= space);
	return [ f(pos[1],model.X), f(pos[2],model.Y), f(pos[3],model.Z) ]
end

"""
	lookupRefIndex(model::RayTraceModel, position)

Lookup the refractive index in a `RayTraceModel` at a `specified position`.
"""
function lookupRefIndex(model::RayTraceModel{T}, pos::Vector{T}) where {T<:AbstractFloat}
	return model.N[lookupVoxel(model,pos)...]
end

"""
	isvalid(model::RayTraceModel)

Validate basic properties of a RayTraceModel.
"""
function isvalid(model::RayTraceModel)
	if false && mapreduce(<(1), +, model.N)
		# TODO this mapreduce causes a StackOverflow
		# 	any( model.N .< 1 ) or similar also failed
		# 	need to find a working way to check for bad values
		# 	temporarily hard-wired to pass
		@error "Model has refractive index data < 1"
		return false
	elseif !all(diff(model.X) .>= 0)
		@error "Model's X axis is not monotonic-increasing"
		return false
	elseif !all(diff(model.Y) .>= 0)
		@error "Model's Y axis is not monotonic-increasing"
		return false
	elseif !all(diff(model.Z) .>= 0)
		@error "Model's Z axis is not monotonic-increasing"
		return false
	elseif !all(isvalid.(model.rays))
		@error "Model has at least one invalid Ray"
		return false
	else
		# No errors detected
		return true
	end
end

"""
	isvalid(ray::Ray)
Validate basic properties of a Ray.
"""
function isvalid(ray::Ray)
	if length(ray.position) != length(ray.direction)
		@error "Ray's position and direction vectors are unequal length"
		return false
	else
		# No errors detected
		return true
	end
end

