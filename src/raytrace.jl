"""
	gradient(model::RayTraceModel, index, w)
Determine the refractive index gradient vector for a particular voxel specified
by the `index [ix, iy, iz]`, using a mean of the finite differences within
a given `window size` along each dimension.

If window extends past a matrix boundary, `gradient` returns a zero vector.
"""
function gradient(model::RayTraceModel{T}, i::Vector{Int}; w::Int=3) where {T<:AbstractFloat}
	if ( w <= 0 )
		# Invalid window size
		throw(ArgumentError("Window size must be a positive integer"))
	elseif any(i .- w .< 1) || any(size(model) .- w .< i)
		# Window extends past matrix boundary, return zero vector
		return Vector{T}([0, 0, 0])
	else
		# Window entirely contained within matrix boundaries
		f(x,y,z,w) = (x-w:x+w, y-w:y+w, z-w:z+w)
		window = view(model.N, f(i[1],i[2],i[3],w)...)
	end

	# Return the average finite difference per dimension within window
	grad(mat,dim) = Statistics.mean(diff(mat,dims=dim));
	return Vector{T}(map(i->grad(window,i), [1,2,3]))
end

"""
	snell(i, n, n1, n2)
Use Snell's Law of Refraction to derive the transmission direction vector of a
ray oriented with an `incident direction vector` on a boundary with a given
`normal vector`, originating from a medium of refractive index `n1`, and entering
a medium of refractive index `n2`.
"""
function snell(i::Vector{T}, n::Vector{T}, n1::T, n2::T) where {T<:AbstractFloat}
	# using LinearAlgebra: dot, norm, normalize

	# If zero normal/gradient -> no direction change
	# Else ensure given vectors are unit vectors
	if norm(n)  == 0
		return i
	else
		i = normalize(i)
		n = normalize(n)
	end

	# Transform vector i into a pair of orthogonal basis vectors
	#   i_para is the component of i parallel to n
	#   i_perp is the component of i perpendicular to n
	i_para = dot(i, n) .* n
	i_perp = i .- i_para

	# If i and n are parallel or orthogonal -> no direction change
	if ( norm(i_perp) == 0 ) || ( norm(i_para) == 0 )
		return i
	end

	# Normalize basis vectors into unit vectors
	a = normalize(i_para)
	b = normalize(i_perp)

	# Calculate output angle using Snell's Law
	#        n1 sin(theta1) = n2 sin(theta2)
	#   (n1/n2) sin(theta1) = sin(theta2)
	#
	# Given that vector i is a unit vector, sin(theta1) = ||i_perp||
	#   (n1/n2) ||i_perp|| = sin(theta2)
	sintheta2 = norm(i_perp) * n1/n2

	# Compute t vector as the linear combination of a and b vectors
	if -1 <= sintheta2 <= 1
		# Refraction at or below the critical angle
		#   t = ( cos(theta2) .* a ) .+ ( sin(theta2) .* b )
		t = ( cos(asin(sintheta2)) .* a ) .+ ( sintheta2 .* b )
	else
		# TODO confirm the following
		# Critical angle exceeded, reflecting
		@warn "Ray exceeded critical angle, reflecting"
		t = ( -1 .* i_para ) .+ ( i_perp )
	end

	return t
end

"""
	raytrace(pos, dir)
Trace a Ray origination at a `position` with a given `direction vector` to the
first intersection it has with a voxel surface. Returns that position.
"""
function raytrace(model::RayTraceModel{T}, pos::Vector{T}, dir::Vector{T}) where {T<:AbstractFloat}
	# Find distance to adjacent voxel surfaces
	dx = getAdjBounds(pos[1], model.X) .- pos[1]
	dy = getAdjBounds(pos[2], model.Y) .- pos[2]
	dz = getAdjBounds(pos[3], model.Z) .- pos[3]

	# Use parametric distance to next surface to project vector
	dt = [dx;dy;dz] ./ dir;
	if !any(dt .>= 0)
		# Pointing outside model space
		@debug "Pointing out of bounds; dt = $(dt)"
		return Vector{T}([NaN, NaN, NaN])
	else
		t = minimum(filter(x->(x>0), dt))
		return Vector{T}( pos .+ (t .* dir) )
	end
end

"""
	advance(model::RayTraceModel, ray::Ray)
Trace a `Ray` contained within a `Model` space to the next bounding surface.
"""
function advance(model::RayTraceModel{T}, ray::Ray{T}) where {T<:AbstractFloat}
	# Read the Ray's latest position and direction
	pos1 = ray.position[end]
	dir1 = ray.direction[end]

	# Trace Ray to next surface and terminate Ray if out of bounds
	pos2 = raytrace(model, pos1, dir1)
	if isOutOfBounds(model, pos2)
		@debug "$(Dates.now())> Position $(pos2) is out of bounds, terminating Ray"
		ray.terminated = true;
		return
	end

	# Get refractive indices for prev and next voxels
	n1 = lookupRefIndex(model, pos1)
	n2 = lookupRefIndex(model, pos2 .+ (nextfloat(zero(T),1) .* dir1))

	# Calculate new direction vector
	normal = gradient(model, lookupVoxel(model,pos2))
	dir2 = snell(dir1, normal, n1, n2)

	# Append findings to Ray
	push!(ray.position, pos2)
	push!(ray.direction, dir2)
end

