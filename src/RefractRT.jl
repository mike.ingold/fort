module RefractRT
	# Dependencies
	## Debugging and event logging
	using Dates
	using Logging
	## Vector operations
	using LinearAlgebra
	using Statistics
	# Visualization
	using Plots
	## Permit Base function overloading
	import Base.length
	import Base.show
	import Base.size

	# Core data structures, Base function overloading
	include("structs_Ray.jl")
	export Ray
	include("structs_RayTraceModel.jl")
	export RayTraceModel

	# Utilities for spatial properties of model
	include("utils_spatial.jl")
	export getAdjBounds,
	       isOutOfBounds,
	       lookupRefIndex,
	       lookupVoxel

	# Ray tracing functions
	include("raytrace.jl")
	export advance,
	       gradient,
	       raytrace,
	       snell 

	# Utilities that manipulate models
	include("utils_models.jl")
	export addray,
	       addplanerays,
	       advanceallrays,
	       clone

	# Data Visualization
	include("visual.jl")
	export plotray
end

